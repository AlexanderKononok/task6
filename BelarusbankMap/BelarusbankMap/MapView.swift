//
//  MapView.swift
//  BelarusbankMap
//
//  Created by Alexander Kononok on 10/18/21.
//

import UIKit
import MapKit
import CoreLocation

class MapView: UIView {
    
    private let mapView = MKMapView()
    private let locationManager = CLLocationManager()
    private let regionInMeters: Double = 3000
    
    func setupMapView(mainView: UIView) {
        mainView.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        mapView.delegate = self
    }
    
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on
        }
    }
    
    private func checkLocationAuthorization() {
        switch locationManager.authorizationStatus {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        case .authorizedAlways:
            break
        @unknown default:
            print("Error: unknown value")
        }
    }
    
    private func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion(center: location,
                                            latitudinalMeters: regionInMeters,
                                            longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
}

extension MapView: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let center = CLLocationCoordinate2D(
            latitude: location.coordinate.latitude,
            longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: center,
                                             latitudinalMeters: regionInMeters,
                                             longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
}

extension MapView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        if let title = annotation.title, title == "Belarusbank atm" {
            annotationView?.image = UIImage(named: "local_atm")
        } else if let title = annotation.title, title == "Belarusbank infobox" {
            annotationView?.image = UIImage(named: "infobox")
        } else if let title = annotation.title, title == "Belarusbank filial" {
            annotationView?.image = UIImage(named: "filial")
        } else if annotation === mapView.userLocation {
            annotationView?.image = UIImage(named: "my_location")
        }
        
        annotationView?.canShowCallout = true
        
        return annotationView
    }
}
