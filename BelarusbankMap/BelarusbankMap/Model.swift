//
//  Model.swift
//  BelarusbankMap
//
//  Created by Alexander Kononok on 10/18/21.
//

import Foundation

struct CoordATM: Codable {
    let latitude: String?
    let longitude: String?
    
    enum CodingKeys: String, CodingKey {
        case latitude = "gps_x"
        case longitude = "gps_y"
    }
}
