//
//  ViewController.swift
//  BelarusbankMap
//
//  Created by Alexander Kononok on 10/14/21.
//

import UIKit
import Dispatch

class ViewController: UIViewController {

    private let mainView = UIView()
    private let map = MapView()
    private let gomelATM = BelarusbankAPI()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        setupMainView()
        map.setupMapView(mainView: mainView)
        map.checkLocationServices()
        gomelATM.getATM()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global(qos: .background).async {}
    }
    
    func setupMainView() {
        view.addSubview(mainView)
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.topAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mainView.leadingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        mainView.trailingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        mainView.bottomAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }

}

