//
//  BelarusbankAPI.swift
//  BelarusbankMap
//
//  Created by Alexander Kononok on 10/18/21.
//

import Foundation
import UIKit

class BelarusbankAPI {
    
    func getATM() {
        let session = URLSession.shared
        let url = "https://belarusbank.by/api/atm?city=%D0%93%D0%BE%D0%BC%D0%B5%D0%BB%D1%8C"
        
        if let url = URL(string: url) {
            let task = session.dataTask(with: url) { data, response, error in
                do {
                    let data = try Data(contentsOf: url)
                    
                    if let coords = try? JSONDecoder().decode([CoordATM].self, from: data) {
                        for coord in coords {
                            if let latitude = coord.latitude, let longitude = coord.longitude {
                                print("latitude: \(latitude), longitude: \(longitude)")
                            }
                        }
                    }
                } catch {
                    print("JSON error: \(error.localizedDescription)")
                }
            }
            
            task.resume()
        }
    }
    
}
